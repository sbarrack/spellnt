## Conditions

- [ ] Do you see your problem listed [here](https://gitlab.com/sbarrack/spellnt/-/issues?sort=popularity&state=all)?
- [ ] Have you tried turning it off and on?
- [ ] Is Tampermonkey your script loader?
    - [ ] Are other scripts running on the same site(s)?
- [ ] Do you have extensions that change the appearance or behavior of the site(s)? e.g. Skribbl Typo, Dark Reader
    - If so, which ones?
- [ ] Are you playing on a recent version of [Chrome](https://wikiless.org/wiki/Google_Chrome_version_history#Anchor_to_the_latest_release.) or [Firefox](https://www.mozilla.org/en-US/firefox/releases/)?
    - If so, which one?
- Which OS do you run?

## Summary

Describe your issue clearly and concicely.

## Steps to reproduce

Thoroughly explain the process that lead to the issue occurring by retracing your steps.

1. This is step one
2. Step two comes after step one
3. You get the idea

## What is the current behavior?

Tell me what following your steps to reproduce results in.

## What is the expected behavior?

Then, tell me what you think should happen instead.

## Reference files

Include any relevant logs, error traces, or screenshots illustrating the problem.

## Potential solution

If any, please suggest any troubleshooting steps that may assist with locating the issue, such as line(s) of errant code.

/label ~bug
/assign @sbarrack

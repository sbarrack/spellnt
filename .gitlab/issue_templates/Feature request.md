## Feature request

1. Consider whether or not the feature you are imagining already exists or if can be accomplished by other means
2. Briefly describe the feature(s) you would like added to the game
    - e.g. A new theme, keyboard shortcut, customization option, drawing tool, etc.

/label ~feature
/assign @sbarrack

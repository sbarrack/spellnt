// Express server
if (process.argv[2] === 'watch') {
    const express = require('express')
    const app = express()
    const path = require('path')
    const cors = require('cors')

    const port = 3000

    app.use(cors())
    app.use(express.json())

    app.get('/', (_req, res) => {
        res.sendFile(path.join(__dirname, 'spellnt.user.js'))
    })

    app.listen(port, () => {
        console.log(`Listening on http://localhost:${port}/`)
    })
}

const fs = require('fs')
const sass = require('node-sass')
const uglify = require('uglify-js')

// Transpile SASS into compressed CSS
const css = sass
    .renderSync({
        file: 'sass/main.scss',
        includePaths: ['sass', 'node_modules/bourbon/core'],
        outputStyle: 'compressed',
        precision: 3,
    })
    .css.toString()
    .slice(0, -1)

// Compress JS and place CSS inline
let js = fs.readFileSync('main.js').toString()
js = uglify.minify(`
const css = document.createElement('style');
css.textContent = '${css}';
css.classList.add('spellnt-styles');
document.head.append(css);
${js}`).code

// Write transpiled code to userscript
fs.writeFileSync(
    'spellnt.user.js',
    `// ==UserScript==
// @name            Spellnt
// @namespace       https://gitlab.com/sbarrack/spellnt
// @version         0.1
// @author          sbarrack
// @description     An unaffiliated script for improving accessibility and UX in drawing game
// @icon            https://sketchful.io/res/favicon/favicon-32x32.png
// @updateURL       https://gitlab.com/sbarrack/spellnt/-/raw/master/spellnt.user.js
// @downloadURL     https://gitlab.com/sbarrack/spellnt/-/raw/master/spellnt.user.js
// @supportURL      https://gitlab.com/sbarrack/spellnt/-/issues?sort=popularity&state=all
// @match           http*://sketchful.io
// @match           http*://sketchful.io/room/*
// @grant           GM_setValue
// @grant           GM_getValue
// @noframes
// ==/UserScript==
${js}`
)

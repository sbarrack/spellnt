# Spellnt

An unaffiliated script for improving accessibility and UX in drawing games

## Install

1. [Install Tampermonkey](https://www.tampermonkey.net/)
2. [Click here](https://gitlab.com/sbarrack/spellnt/-/raw/master/spellnt.user.js) to install the userscript
3. Enable automatic updates

## Develop

- [Tampermonkey manual](https://www.tampermonkey.net/documentation.php)

### Setup

1. `git clone git@gitlab.com:sbarrack/spellnt.git`
2. `cd spellnt && npm i`
3. Copy-paste the code below into a blank Tampermonkey script, then save

```
// ==UserScript==
// @name            Spellnt Dev
// @namespace       https://gitlab.com/sbarrack/spellnt
// @version         0.0
// @author          sbarrack
// @description     The development script for Spellnt
// @icon            https://sketchful.io/res/favicon/favicon-32x32.png
// @match           http*://sketchful.io
// @match           http*://sketchful.io/room/*
// @require         http://localhost:3000/
// @grant           GM_setValue
// @grant           GM_getValue
// @noframes
// ==/UserScript==
```

### Workflow

1. `npm run watch`

// ==UserScript==
// @name            Spellnt
// @namespace       https://gitlab.com/sbarrack/spellnt
// @version         0.1
// @author          sbarrack
// @description     An unaffiliated script for improving accessibility and UX in drawing game
// @icon            https://sketchful.io/res/favicon/favicon-32x32.png
// @updateURL       https://gitlab.com/sbarrack/spellnt/-/raw/master/spellnt.user.js
// @downloadURL     https://gitlab.com/sbarrack/spellnt/-/raw/master/spellnt.user.js
// @supportURL      https://gitlab.com/sbarrack/spellnt/-/issues?sort=popularity&state=all
// @match           http*://skribbl.io/*
// @match           http*://sketchful.io
// @match           http*://sketchful.io/room/*
// @exclude         http*://skribbl.io/credits
// @exclude         http*://skribbl.io/terms
// @grant           GM_setValue
// @grant           GM_getValue
// @noframes
// ==/UserScript==
const css=document.createElement("style");switch(css.textContent='/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}main{display:block}h1{font-size:2em;margin:0.67em 0}hr{box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace, monospace;font-size:1em}a{background-color:transparent}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace, monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-0.25em}sup{top:-0.5em}img{border-style:none}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}button,[type="button"],[type="reset"],[type="submit"]{-webkit-appearance:button}button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner{border-style:none;padding:0}button:-moz-focusring,[type="button"]:-moz-focusring,[type="reset"]:-moz-focusring,[type="submit"]:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:0.35em 0.75em 0.625em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{vertical-align:baseline}textarea{overflow:auto}[type="checkbox"],[type="radio"]{box-sizing:border-box;padding:0}[type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button{height:auto}[type="search"]{-webkit-appearance:textfield;outline-offset:-2px}[type="search"]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details{display:block}summary{display:list-item}template{display:none}[hidden]{display:none}.spellnt-is-sketchful .html{background-color:#0b5dea;background-image:url(https://sketchful.io/8519a42080077ff3e9a9.png)}.spellnt-is-sketchful .header a div{display:inline-block;height:8rem;width:24rem;color:transparent;background-image:url("/022a7794c0f2ba1d6a52.png");background-size:contain;background-repeat:no-repeat;background-position:center;margin-left:auto;margin-right:auto}.spellnt-is-sketchful .loading{position:fixed;top:50%;left:50%;z-index:1;transform:translate(-50%, -50%)}.spellnt-is-sketchful .modal-dialog{display:inline-block;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%)}.spellnt-is-sketchful .modal-content{background-color:#fff}.spellnt-is-sketchful .modal-title{display:inline-block}.spellnt-is-sketchful .close{float:right}.spellnt-is-sketchful .menuNav{background-color:#fff}.spellnt-is-sketchful .menuNav ul{list-style-type:none;padding-left:0;margin-bottom:0}.spellnt-is-sketchful .menuNavRightLogin a{color:transparent}.spellnt-is-sketchful ul.nav{list-style:none;padding-left:0;margin-bottom:0}.spellnt-is-sketchful .menu{background:#fff;overflow:hidden}.spellnt-is-sketchful .hat,.spellnt-is-sketchful .eyes,.spellnt-is-sketchful .mouth,.spellnt-is-sketchful .body{position:absolute;width:198px;height:198px;background-repeat:no-repeat;background-size:auto}',css.classList.add("spellnt-styles"),document.head.append(css),location.host){case"skribbl.io":document.body.classList.add("spellnt-is-skribbl");const a=M=>{const A=[16777215,12698049,15667979,16740608,16770048,52224,45823,2301907,10682554,13860010,10506797,0,5000268,7604999,12728320,15245824,21776,22174,919653,5570665,10966388,6500365],z=["rgb(255, 255, 255)","rgb(193, 193, 193)","rgb(239, 19, 11)","rgb(255, 113, 0)","rgb(255, 228, 0)","rgb(0, 204, 0)","rgb(0, 178, 255)","rgb(35, 31, 211)","rgb(163, 0, 186)","rgb(211, 124, 170)","rgb(160, 82, 45)","rgb(0, 0, 0)","rgb(76, 76, 76)","rgb(116, 11, 7)","rgb(194, 56, 0)","rgb(232, 162, 0)","rgb(0, 85, 16)","rgb(0, 86, 158)","rgb(14, 8, 101)","rgb(85, 0, 105)","rgb(167, 85, 116)","rgb(99, 48, 13)"],e=["scsChatFocus","scsChatFocus2","scsChatFocus2","scsGamemode","scsBrushSize","scsBrushColor","scsRainbowMode","scsRainbowSpeed"],S={},k=(e.forEach(e=>S[e]=localStorage.getItem(e)),addEventListener("beforeunload",()=>{e.forEach(e=>{S[e]&&"null"!==S[e]?localStorage.setItem(e,S[e]):localStorage.removeItem(e)})}),{}),x={x:null,y:null},N=[];let R=11,w,E,q,P,B,G,L,I,D,F,O="",C,T,$,W;function H(){var e=z.indexOf(T.style.backgroundColor);T.style.backgroundColor=C.style.backgroundColor,-1!=e&&L[e].click()}function j(){const e=document.getElementById("scsPostWrapper");e&&(e.setAttribute("title","Save the current image"),M(e).tooltip()),document.getElementById("scsPostAwesome").addEventListener("click",()=>{var e=w.toDataURL().split(",")[1],t=E.innerText;if(n)clearTimeout(n),n=setTimeout(s,3e3);else{n=setTimeout(s,3e3);const o=new FormData;o.append("image",e),o.append("name",Date.now()+".png"),o.append("title",t+" by "+D),o.append("description","Saved image from skribbl.io"),fetch("https://api.imgur.com/3/image",{method:"POST",headers:new Headers({Authorization:"Client-ID b5db76b67498dd6"}),body:o}).then(e=>{e.json().then(e=>{alert(e.data.link)}).catch(e=>{console.debug(e),alert("Failed to send image to imgur :(")})}).catch(console.debug)}});let n;function s(){clearTimeout(n),n=0}}function U(){$&&x.x&&x.y&&document.dispatchEvent(new MouseEvent("mousemove",{bubbles:!0,cancelable:!0,clientX:x.x,clientY:x.y}))}function K(){let e=E.innerText;e?((e=e.split(" ")).forEach((e,t,o)=>{o[t]=e.replaceAll("-","").length}),F.innerHTML=`&nbsp;(${e.join(",")})`):F.innerHTML=""}function t(){w=document.getElementById("canvasGame"),E=document.getElementById("currentWord"),B=document.getElementById("inputChat"),(F=document.createElement("div")).id="scsWordSize",E.parentNode.insertBefore(F,E.nextSibling);const e=document.createElement("div"),d=(e.classList.add("scsTitleMenu"),e.innerHTML=`
                <div>
                <label for="scsGamemode">Gamemode:</label>
                <select class="form-control" id="scsGamemode">
                    <option>None</option>
                    <option>Blind</option>
                    <option>Deaf</option>
                    <option>One shot</option>
                </select>
                </div>
                <h5>Keybinds</h5>
                <p><i>Esc</i> unbinds a key binding.</p>
                <div>
                <label for="scsChatFocus">Focus chat:</label>
                <select class="form-control" id="scsChatFocus">
                    <option>None</option>
                    <option>Shift</option>
                    <option>Alt</option>
                    <option>Ctrl</option>
                </select>
                <h5 class="plus">+</h5>
                <input class="form-control" id="scsChatFocus2" placeholder="Click to bind..." readonly>
                </div>
                <div>
                <label for="scsBrushSize">Brush size:</label>
                <select class="form-control" id="scsBrushSize">
                    <option>None</option>
                    <option>1-4</option>
                    <option>Numpad 1-4</option>
                </select>
                <label for="scsBrushColor">Brush color:</label>
                <select class="form-control" id="scsBrushColor">
                    <option>None</option>
                    <option>0-9</option>
                    <option>Numpad 0-9</option>
                </select>
                </div>

                <style>
                .scsTitleMenu {
                    background-color: #fff;
                    border-radius: 2px;
                    padding: 8px;
                    margin-top: 20px;
                    margin-bottom: 20px;
                }
                .scsTitleMenu > div { display: flex; margin-bottom: 10px; }
                .scsTitleMenu > h4, .scsTitleMenu > h5, .scsTitleMenu > p { text-align: center; }
                .scsTitleMenu p { font-size: 12px; }
                .scsTitleMenu h5 { font-size: 16px; }
                .scsTitleMenu h5.plus { margin-left: 10px; font-weight: bold; }
                .scsTitleMenu label {
                    vertical-align: middle;
                    align-self: center;
                    margin-bottom: 0;
                }
                .scsTitleMenu > div > label:nth-child(n + 2) {
                    margin-left: 10px;
                }
                .scsTitleMenu .form-control {
                    margin-left: 10px;
                    width: auto;
                }
                </style>`,document.querySelector("#screenLogin > .login-content > .loginPanelContent")),m=(d.parentNode.insertBefore(e,d.nextSibling),document.querySelector('[data-tool="pen"] > .toolIcon')),u=(m.setAttribute("title","(B)rush (middle click to pick colors)"),M(m).tooltip("fixTitle"),document.getElementById("containerFreespace"));u.innerHTML=`<div id="scsCustomUi">
                <div id="scsPostWrapper" data-toggle="tooltip" data-placement="top" title="Save the current image">
                <button id="scsPostAwesome" class="btn btn-success btn-xs scsPost">
                    Save Drawing
                </button>
                </div>
                <div id="scsRainbowWrapper">
                <span>Brush mode:</span>
                <select class="form-control" id="scsRainbowMode" value="1-color">
                    <option>1-color</option>
                    <option>2-cycle</option>
                    <option>Light</option>
                    <option>Dark</option>
                    <option>All</option>
                    <option>Gray</option>
                </select>
                <span>Speed (ms):</span>
                <input type="number" id="scsRainbowSpeed" class="form-control" min="10" max="1000" value="50" step="10" size="4" maxlength="4">
                </div>

                <style>
                #rateDrawing { position: fixed; }
                div#currentWord { text-align: right; }
                #scsWordSize {
                    flex: 1 1 auto;
                    font-size: 28px;
                    font-weight: 700;
                    letter-spacing: 3px;
                }
                input::-webkit-outer-spin-button,
                input::-webkit-inner-spin-button {
                    -webkit-appearance: none;
                    margin: 0;
                }
                #containerBoard .containerToolbar { display: flex !important }
                #scsCustomUi { color: white; }
                #scsCustomUi > div { margin-bottom: 10px; display: flex; align-items: center; justify-content: space-between; }
                .scsPost { position: relative; width: 100%; }
                #scsPostWrapper.disabled > * {
                    opacity: 0.7;
                    pointer-events: none;
                }
                #scsRainbowWrapper { margin-bottom: 10px; font-size: 12px; }
                #scsRainbowWrapper .form-control { width: auto; }
                .containerTools .tool[data-tool^="scs"].scsToolActive {
                    background-color: #559105;
                    filter: none;
                }
                .containerTools .tool[data-tool^="scs"]:hover {
                    background-color: #699b37;
                    filter: none;
                }
                div.colorPreview {
                    width: 32px;
                    height: 32px;
                    margin-right: 24px;
                }
                .scsColorPreview {
                    top: 16px;
                    left: 16px;
                    position: relative;
                    width: 32px;
                    height: 32px;
                    z-index: -1;
                    border-radius: 2px;
                }
                #randomIcon {
                    display: none;
                }
                #containerPlayerlist .player .name:hover {
                    cursor: pointer;
                    text-decoration: underline;
                }
                #containerPlayerlist .player {
                    max-height: 48px;
                }
                .scsMute {
                    opacity: 0.5;
                }
                .scsMute .message,
                .scsDeaf .message {
                    display: none !important;
                }
                .scsDeaf #boxMessages {
                    opacity: 0;
                }
                [scsMuteSender] {
                    display: none;
                }
                </style>
                </div>`,u.style.background="none",C=document.getElementsByClassName("colorPreview")[0],(T=C.cloneNode()).classList.add("scsColorPreview"),T.classList.remove("colorPreview"),T.style.backgroundColor=z[0],T=C.appendChild(T),C.setAttribute("title","Color (T)oggle"),M(C).tooltip("fixTitle"),C.addEventListener("click",H);{const t=document.getElementById("scsChatFocus"),o=(q=S.scsChatFocus,t.value=q||"None",t.addEventListener("change",e=>{S.scsChatFocus=e.target.value,q=e.target.value}),document.getElementById("scsChatFocus2"));P=S.scsChatFocus2,o.value=P,o.addEventListener("click",()=>{function t(e){P="Escape"!==e.key?(S.scsChatFocus2=e.key,e.target.value=e.key,e.key):(S.scsChatFocus2="",e.target.value=""),document.removeEventListener("keydown",t)}document.addEventListener("keydown",t),setTimeout(()=>{document.removeEventListener("keydown",t)},1e4)})}j();{I=sessionStorage.getItem("scsGamemode");const n=document.getElementById("scsGamemode");n.value=I||"None",n.addEventListener("change",e=>{sessionStorage.setItem("scsGamemode",e.target.value),I=e.target.value})}{const s=document.getElementById("scsBrushColor"),l=(s.value=S.scsBrushColor||"None",document.getElementById("scsBrushSize"));l.value=S.scsBrushSize||"None",L=document.querySelectorAll("[data-color]"),G=document.querySelectorAll("[data-size]"),l.addEventListener("change",e=>S.scsBrushSize=e.target.value),s.addEventListener("change",e=>S.scsBrushColor=e.target.value)}{const i=document.createElement("img"),a=(i.id="scsAnchor",i.style.display="none",i.style.position="absolute",i.style.pointerEvents="none",i.src="https://raw.githubusercontent.com/sbarrack/skribbl-community-scripts/master/images/anchor.png",document.body.appendChild(i),k.scsAnchor=i,document.querySelector('[data-tool="erase"]'));let t=a.cloneNode(!0);t.setAttribute("data-tool","scsHatching"),t.firstChild.setAttribute("title","(H)atching (middle click to anchor, space to unanchor)"),t.firstChild.setAttribute("src","https://raw.githubusercontent.com/sbarrack/skribbl-community-scripts/master/images/hatchet.gif"),t=a.parentNode.insertBefore(t,a),M(t.firstChild).tooltip(),t.addEventListener("click",()=>{t.classList.toggle("scsToolActive"),t.classList.contains("scsToolActive")?(x.x&&x.y&&(i.style.display="block"),W=setInterval(U,S.scsRainbowSpeed)):(i.style.display="none",W&&(clearInterval(W),W=0))}),document.addEventListener("mousedown",e=>{t.classList.contains("scsToolActive")&&(0==e.button?$=!0:1==e.button&&(x.x=e.clientX,x.y=e.clientY,i.style.display="block",i.style.top=e.clientY-4+"px",i.style.left=(e.clientX-13).toString(10)+"px"))}),document.addEventListener("mouseup",e=>{t.classList.contains("scsToolActive")&&0==e.button&&($=!1)}),k.hatchingTool=t}{let t=0;const g=[0,1,12,11];function p(){var e;"1-color"===S.scsRainbowMode?(e=z.indexOf(C.style.backgroundColor),L[(e+11)%22].click()):"2-cycle"===S.scsRainbowMode?H():"Light"===S.scsRainbowMode?L[t%7+2].click():"Dark"===S.scsRainbowMode?L[t%7+13].click():"Gray"===S.scsRainbowMode?L[g[t%4]].click():"All"===S.scsRainbowMode&&L[t%22].click(),t=(t+1)%22}const c=document.querySelector('[data-tool="erase"]');let o=c.cloneNode(!0);o.setAttribute("data-tool","scsRainbow"),o.firstChild.setAttribute("title","Magic b(R)ush"),o.firstChild.setAttribute("src","https://raw.githubusercontent.com/sbarrack/skribbl-community-scripts/master/images/brush.gif"),o=c.parentNode.insertBefore(o,c),M(o.firstChild).tooltip();let n;o.addEventListener("click",e=>{o.classList.toggle("scsToolActive"),o.classList.contains("scsToolActive")?n=setInterval(p,S.scsRainbowSpeed):n&&(clearInterval(n),n=null)}),k.rainbowTool=o;const b=document.getElementById("scsRainbowMode"),r=(b.value=S.scsRainbowMode||"1-cycle",b.addEventListener("change",e=>S.scsRainbowMode=e.target.value),document.getElementById("scsRainbowSpeed"));S.scsRainbowSpeed=parseInt(S.scsRainbowSpeed)||50,r.value=S.scsRainbowSpeed,r.addEventListener("change",e=>S.scsRainbowSpeed=e.target.value),r.addEventListener("change",e=>{S.scsRainbowSpeed=parseInt(e.target.value),n&&(clearInterval(n),n=setInterval(p,S.scsRainbowSpeed)),W&&(clearInterval(W),W=setInterval(U,S.scsRainbowSpeed)),r.blur()}),k.rainbowSpeed=r}document.addEventListener("click",e=>{if(e.target.classList.contains("name")&&e.target.closest("#containerGamePlayers")){e.stopImmediatePropagation();const n=e.target.innerText;var t=N.indexOf(n);-1==t?(N.push(n),e.target.parentElement.parentElement.classList.add("scsMute"),Array.from(document.querySelectorAll("#boxMessages > p > b")).forEach((e,t,o)=>{e.innerText.endsWith(": ")&&e.innerText.slice(0,-2)===n&&e.parentElement.setAttribute("scsMuteSender",n)})):(N.splice(t,1),e.target.parentElement.parentElement.classList.remove("scsMute"),t=n.replace(/["\\]/g,"\\$&"),Array.from(document.querySelectorAll(`[scsMuteSender="${t}"]`)).forEach((e,t,o)=>{e.removeAttribute("scsMuteSender")}))}});{const h=new MutationObserver(e=>{if("none"!==e[0].target.style.display){const o=Array.from(document.querySelectorAll(".drawing")).find(e=>e.offsetParent);function t(e){"Enter"===e.key&&(B.disabled=!0,B.removeEventListener("keyup",t))}o&&(D=o.closest(".player").querySelector(".name").innerHTML),"Blind"===I?w.style.opacity=0:w.style.opacity=1,"Deaf"===I?(document.getElementsByClassName("containerGame")[0].classList.add("scsDeaf"),E.style.opacity=0):(document.getElementsByClassName("containerGame")[0].classList.remove("scsDeaf"),E.style.opacity=1,K()),"One shot"===I&&B.addEventListener("keyup",t);const n=new MutationObserver(e=>{"none"!==e[0].target.style.display?"Deaf"===I&&(B.disabled=!1,B.removeEventListener("keyup",t)):"Deaf"!==I?K():B.addEventListener("keyup",t)});n.observe(document.getElementById("overlay"),{attributes:!0,attributeFilter:["style"]})}}),y=(h.observe(document.getElementById("screenGame"),{attributes:!0,attributeFilter:["style"]}),new MutationObserver(e=>{const t=e[0].target;"none"!==t.style.display&&setTimeout(()=>{D=t.closest(".player").querySelector(".name").innerHTML},3e3)})),v=new MutationObserver(e=>{if(1<e.length)document.querySelectorAll(".drawing").forEach(e=>{y.observe(e,{attributes:!0,attributeFilter:["style"]})});else if(0<e[0].addedNodes.length){const t=e[0].addedNodes[0];y.observe(t.querySelector(".avatar .drawing"),{attributes:!0,attributeFilter:["style"]})}}),f=(v.observe(document.getElementById("containerGamePlayers"),{childList:!0}),new MutationObserver(e=>{e.forEach(e=>{e.addedNodes.forEach(e=>{const t=e.firstChild.innerText;var o=t.slice(0,-2);t.endsWith(": ")&&N.includes(o)&&e.setAttribute("scsMuteSender",o)})})}));f.observe(document.getElementById("boxMessages"),{childList:!0})}document.addEventListener("keydown",t=>{if("INPUT"!==document.activeElement.tagName){{var o=t;let e=!0;"Shift"===q?e=o.shiftKey:"Alt"===q?e=o.altKey:"Ctrl"===q&&(e=o.ctrlKey),!e||P&&o.key!==P||(o.preventDefault(),B.focus())}"r"===(o=t).key?k.rainbowTool.click():"t"===o.key?H():"h"===o.key?k.hatchingTool.click():" "===o.key&&k.hatchingTool.classList.contains("scsToolActive")&&(o.preventDefault(),o.stopPropagation(),Object.assign(x,{x:null,y:null}),k.scsAnchor.style.display="none"),["1","2","3","4"].includes((o=t).key)&&("1-4"===S.scsBrushSize&&o.code.match(/Digit[0-9]/)||"Numpad 1-4"===S.scsBrushSize&&o.code.match(/Numpad[0-9]/))&&G[+o.key-1].click();o=t;if(["0","1","2","3","4","5","6","7","8","9"].includes(o.key)&&("0-9"===S.scsBrushColor&&o.code.match(/Digit[0-9]/)||"Numpad 0-9"===S.scsBrushColor&&o.code.match(/Numpad[0-9]/))){let e=11;if("0"===o.key)switch(R){case 11:e=0;break;case 0:e=1;break;case 1:e=12}else e=R==+o.key+1?+o.key+12:+o.key+1;L[e].click(),R=e}}else"inputChat"===document.activeElement.id&&("ArrowUp"===t.key?B.value=O:"Enter"===t.key&&(O=B.value))}),w.addEventListener("mousedown",e=>{var t;1!=e.button||k.hatchingTool.classList.contains("scsToolActive")||(t=w.getBoundingClientRect(),e=Uint32Array.from(w.getContext("2d").getImageData(Math.floor((e.clientX-t.x)/t.width*w.width),Math.floor((e.clientY-t.y)/t.height*w.height),1,1).data),-1!=(t=A.indexOf(e[0]<<16|e[1]<<8|e[2]))&&L[t].click())})}"complete"===document.readyState?t():window.addEventListener("load",t)};break;case"sketchful.io":document.body.classList.add("spellnt-is-sketchful");const b="Spellnt";let n=9,e=(new MutationObserver((e,t)=>{for(const o of e)o.addedNodes.length&&(document.querySelectorAll("#menuLogin .nav-item:nth-last-child(n + 2), #menuLoginTabs > .tab-pane:nth-child(n + 2)").forEach(e=>{e.remove(),n--}),document.querySelectorAll('style[type="text/css"]').forEach(e=>{e.remove(),n--})),o.attributeName&&"menu"==o.target.id&&(o.target.style.transform="",n--);n<=0&&(delete n,t.disconnect())}).observe(document,{subtree:!0,childList:!0,attributes:!0,attributeFilter:["style"]}),document.body.querySelector('script[src^="https://www.google"]'));e.nextElementSibling.remove(),e.remove(),(e=document.head.querySelector("script[async]")).previousElementSibling.remove(),e.remove(),delete e,document.head.querySelector('script[src^="js/mobile."]').remove();let t;document.querySelectorAll("style:not(#extraCSS):not(.spellnt-styles)").forEach(e=>{/Bootstrap/g.test(e.textContent)?t=e:e.remove()}),document.querySelector("#menuPlay > .menuColumn:last-child").remove(),document.querySelector('a[href="#menuShop"]').parentElement.remove(),document.querySelector('a[href="#menuGallery"]').parentElement.remove(),document.querySelector('a[href="#menuRankings"]').parentElement.remove(),document.getElementById("socialDiscord").remove(),document.getElementById("socialShare").remove(),document.getElementById("socialIcons").remove(),document.getElementById("news").remove(),document.getElementById("menuSettingsTutorialIcon").parentElement.remove(),document.getElementById("tutorialModal").remove(),document.getElementById("alertPrivate").remove(),document.getElementById("featuredStreamers").remove(),document.getElementById("menuShop").remove(),document.getElementById("menuRankings").remove(),document.getElementById("menuComingSoon").remove(),document.getElementById("blockTest").remove(),document.getElementById("preroll").remove(),document.getElementById("moneySide").remove(),document.getElementById("moneyBottom").remove(),document.getElementById("menuGallery").remove(),document.getElementById("moneySideGame").remove(),document.getElementById("moneyTopGame").remove(),document.getElementById("moneyBottomGame").remove(),document.querySelector(".rotate").remove(),document.querySelectorAll(".mobileOnly").forEach(e=>{e.remove()}),t.textContent=t.textContent.replace(/^[\s\S\n]*?\}/,"").replace(/html\s?\{[\s\S\n]*?\}/g,"").replace(/(?<=\}\n?\s*?)\*\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)body\s?\{[\s\S\n]*?\}/g,"").replace(/(?<=\}\n?\s*?)img\s?\{[\s\S\n]*?\}/g,"").replace(/(?<=\}\n?\s*?)(?:p|hr|b\,\s?strong)\s?\{[\s\S]*?\}/g,"").replace(/\.loading[\s\S\n]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.fade\s?\{[\s\S]*?\}/g,"").replace(/(?<=[\}|\{]\n?\s*?)\.modal-dialog(?:-centered(?:::before)?)?\s?\{[\s\S\n]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.modal-(?:content|header|body|title)\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.modal-footer(?:\s?>\s?\*)?\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)(?:\.modal-header\s|button)?.close\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)#alertModal\s?\{[\s\S]*?\}/g,"").replace(/(?<=[\}|\{]\n?\s*?)\.login[\s\S\n]*?\}/g,"").replace(/#menu\s?\{[\s\S\n]*?(?=\.menu|#menuL)/g,"").replace(/\.header\s?\{[\s\S\n]*?(?=\.nav-icon|#menuL)/g,"").replace(/(?<=\}\n?\s*?)#logoContainer\s?\{[\s\S]*?\}/g,"").replace(/#menuLogo\s?\{[\s\S\n]*?(?=\.gameLogo|\.menu)/g,"").replace(/(?<=\}\n?\s*?).text-center\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?).menuNav(?:AvatarCircle|Right(?:Button|Login(?:Link|\sa(?::(?:hover|active)\s?\+\s?.menuNavRightButton)?)?)?|\sul(?:\sli(?:\sa(?:\.active|:hover|\simg|\sspan|:not\(\.active\):hover\sspan)?)?)?)?\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)#menuNavAvatar|\.(?:avatar(?:Sprite)?|hat|eyes|mouth|body)?\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?).menu\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.menuTabs\s?>\s?div:not\(\.active\)\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)#(?:menuAbout(?:\simg)?|menuLogin)?\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.tab-content\s?>\s?\.(?:active|tab-pane)?\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)(?:\.nav-pills\s)?\.nav-link\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.nav\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.nav-fill\s?>\s?\.nav-link,\n?\s*?\.nav-fill\s\.nav-item\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.form-control\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.mt-0,\n?\s*?\.my-0\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.media-body\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.mr-3,\n?\s*?\.mx-3\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)(?:#menuSettings\s)?\.media\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)#menuSettingsVolume\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.form-control-file,\n?\s*?\.form-control-range\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)(?:#menuSettings\s)?\.col\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.col-xl,\n?\s*?\.col-xl-auto,\n?\s*?\.col-xl-12,\n?\s*?\.col-xl-11,\n?\s*?\.col-xl-10,\n?\s*?\.col-xl-9,\n?\s*?\.col-xl-8,\n?\s*?\.col-xl-7,\n?\s*?\.col-xl-6,\n?\s*?\.col-xl-5,\n?\s*?\.col-xl-4,\n?\s*?\.col-xl-3,\n?\s*?\.col-xl-2,\n?\s*?\.col-xl-1,\n?\s*?\.col-lg,\n?\s*?\.col-lg-auto,\n?\s*?\.col-lg-12,\n?\s*?\.col-lg-11,\n?\s*?\.col-lg-10,\n?\s*?\.col-lg-9,\n?\s*?\.col-lg-8,\n?\s*?\.col-lg-7,\n?\s*?\.col-lg-6,\n?\s*?\.col-lg-5,\n?\s*?\.col-lg-4,\n?\s*?\.col-lg-3,\n?\s*?\.col-lg-2,\n?\s*?\.col-lg-1,\n?\s*?\.col-md,\n?\s*?\.col-md-auto,\n?\s*?\.col-md-12,\n?\s*?\.col-md-11,\n?\s*?\.col-md-10,\n?\s*?\.col-md-9,\n?\s*?\.col-md-8,\n?\s*?\.col-md-7,\n?\s*?\.col-md-6,\n?\s*?\.col-md-5,\n?\s*?\.col-md-4,\n?\s*?\.col-md-3,\n?\s*?\.col-md-2,\n?\s*?\.col-md-1,\n?\s*?\.col-sm,\n?\s*?\.col-sm-auto,\n?\s*?\.col-sm-12,\n?\s*?\.col-sm-11,\n?\s*?\.col-sm-10,\n?\s*?\.col-sm-9,\n?\s*?\.col-sm-8,\n?\s*?\.col-sm-7,\n?\s*?\.col-sm-6,\n?\s*?\.col-sm-5,\n?\s*?\.col-sm-4,\n?\s*?\.col-sm-3,\n?\s*?\.col-sm-2,\n?\s*?\.col-sm-1,\n?\s*?\.col,\n?\s*?\.col-auto,\n?\s*?\.col-12,\n?\s*?\.col-11,\n?\s*?\.col-10,\n?\s*?\.col-9,\n?\s*?\.col-8,\n?\s*?\.col-7,\n?\s*?\.col-6,\n?\s*?\.col-5,\n?\s*?\.col-4,\n?\s*?\.col-3,\n?\s*?\.col-2,\n?\s*?\.col-1\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.row\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)\.justify-content-center\s?\{[\s\S]*?\}/g,"").replace(/(?<=\}\n?\s*?)#menu(?:Settings|Lobbies|Play|Lobbies(?:Spinner|Table))\s?\{[\s\S]*?\}/g,"");const f=document.head.querySelector("title"),g=f.textContent.split(" - ")[0];f.textContent=b,delete f,document.getElementById("menuAbout").insertAdjacentHTML("beforeend",`<p>&zwnj;<br><b>${b}, made with love ❤️ by Kallui</b></p><hr><p>
                The userscript ${b} is not an Iframe of ${g}. ${b} is
                not designed to modify or disrupt the ${g} experience or other
                user's experience. ${b} is not made for the purpose of disrupting
                the service or to undermine the legitimate operation of the game
                client. ${b} is not associated with, approved by, or endorsed by
                ${g}. All Intellectual Property Rights in the site and the ${g}
                Game belong to their respective owners. Use ${b} at your own
                discretion and with careful consideration of the Terms of Service of
                ${g}.
            </p><hr>`),delete g,GM_getValue("loadingImg")||GM_setValue("loadingImg","/res/icons/loading.png"),document.querySelector(".loading > img").src=GM_getValue("loadingImg","/res/icons/loading.png")}